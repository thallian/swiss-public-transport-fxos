var gulp = require('gulp');
var zip = require('gulp-zip');

module.exports = function(name) {
  return gulp.src('dist/**/*')
    .pipe(zip(name))
    .pipe(gulp.dest('packaged'));
};
