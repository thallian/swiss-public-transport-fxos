var fs = require('fs-extra');
var gulp = require('gulp');

module.exports = function() {
  fs.removeSync('./dist');
  fs.mkdirsSync('./dist');
  fs.copySync('./src/static/.', './dist/');
};
