var gulp = require('gulp');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');

module.exports = function(paths) {
  paths = paths || [];
  paths.push('./src/css/**/*');
  return gulp.src(paths)
    .pipe(stylus({
      compress: true
    }))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('./dist/css'));
};
