import rest from 'rest';
import mime from 'rest/interceptor/mime';
import moment from 'moment';

export default class TransportApi {
  constructor() {
    let endpoint = localStorage.getItem('apiurl');

    this.endpoint = endpoint.endsWith('/') ? endpoint.substr(0, endpoint.length - 1) : endpoint;
    this.client = rest.wrap(mime);
  }

  reachable() {
    return true; // might have to change that...
  }

  locations(query) {
    return this.client(`${this.endpoint}/locations?query=${query}`).then(function(response) {
      return response.entity.stations;
    });
  }

  connections(from, to, datetime, isArrivalTime) {
    let dateParam = datetime.format('YYYY-MM-DD');
    let timeParam = datetime.format('HH:mm');
    let isArrivalTimeParam = isArrivalTime ? '1' : '0';

    return this.client(`${this.endpoint}/connections?from=${from}&to=${to}&date=${dateParam}&time=${timeParam}&isArrivalTime=${isArrivalTimeParam}`).then(function(response) {
      return response.entity.connections;
    });
  }
}
