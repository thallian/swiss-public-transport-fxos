import moment from 'moment';

angular.module('spt.filters', [])

  .filter('datetime', function($translate) {
    return function(input, id) {
      let datetime = moment.unix(input);
      let formatString = $translate.instant(id);

      return datetime.format(formatString);
    };
  })
  .filter('duration', function($translate) {
    return function(input) {
      let re = /(\d{1,2})d(\d{1,2}):(\d{1,2}):(\d{1,2})/;
      let results = re.exec(input);

      return input ? results[2] + ':' + results[3] : undefined;
    };
  })
  .filter('departure', function($translate) {
    return function(section) {
      let formatString = $translate.instant('FORMAT_TIME_SHORT');
      let departureTime = moment.unix(section.departure.departureTimestamp).format(formatString);

      let platformString = '';
      if (section.departure.prognosis.platform || section.departure.platform) {
          platformString = `, ${$translate.instant('CONNECTION_TEXT_PLATFORM')} ${section.departure.prognosis.platform ? section.departure.prognosis.platform : section.departure.platform}`;
      }

      let departureString = `${section.departure.location.name}, ${departureTime}${platformString}`;

      return departureString;
    };
  })
  .filter('arrival', function($translate) {
    return function(section) {
      let formatString = $translate.instant('FORMAT_TIME_SHORT');
      let arrivalTime = moment.unix(section.arrival.arrivalTimestamp).format(formatString);

      let platformString = '';
      if (section.arrival.prognosis.platform || section.arrival.platform) {
          platformString = `, ${$translate.instant('CONNECTION_TEXT_PLATFORM')} ${section.arrival.prognosis.platform ? section.arrival.prognosis.platform : section.arrival.platform}`;
      }

      let arrivalString = `${section.arrival.location.name}, ${arrivalTime}${platformString}`;

      return arrivalString;
    };
  })
  .filter('walking', function($translate) {
    return function(durationString) {
      let duration = moment(durationString, 'HH:mm:ss');

      let walkingString = $translate.instant('CONNECTION_TEXT_WALKING', { duration: duration.minutes() });

      return walkingString;
    };
  });
