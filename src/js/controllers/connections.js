import moment from 'moment';
import Api from '../transport';

export default function connectionsCtrl($scope, $stateParams, $ionicLoading, $ionicPopup, $translate, appModalService) {
  $scope.title = `${$stateParams.from} - ${$stateParams.to}`;
  $scope.data = {
    connections: []
  };

  $ionicLoading.show();

  let api = new Api();
  api.connections($stateParams.from, $stateParams.to, moment.unix($stateParams.timestamp), $stateParams.isArrivalTime === '1').then(function(connections) {
    $ionicLoading.hide();

    $scope.data.connections = connections;
  }).catch(function(exception) {
    console.log(exception);

    $ionicPopup.alert({
      title: 'Error',
      template: exception.error
    });
    $ionicLoading.hide();
  });

  $scope.details = function(connection) {
    appModalService
      .show('templates/connection.html', 'ConnectionCtrl', {
        connection: connection
      })
      .then(function(result) {}, function(err) {
        // error
      });
  };
}
