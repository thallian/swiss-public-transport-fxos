export default function singleSelectCtrl($scope, $translate, parameters) {
  $scope.data = {
    items: parameters.items,
    selectedItem: parameters.selectedItem || parameters.items[0]
  };

  $translate(parameters.titleId).then(function(title) {
    $scope.title = title;
  });

  $scope.ok = function() {
    $scope.closeModal($scope.data.selectedItem)
  };
}
