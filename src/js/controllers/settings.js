export default function settingsCtrl($scope, $translate, appModalService) {
  $scope.settings = {
    apiUrl: localStorage.getItem('apiurl'),
    languages: [{
      key: 'en',
      name: 'English'
    }, {
      key: 'de',
      name: 'Deutsch'
    }, {
      key: 'fr',
      name: 'Français'
    }, {
      key: 'es',
      name: 'Español'
    }]
  };

  let language = localStorage.getItem('language');

  for (let i = 0; i < $scope.settings.languages.length; i++) {
    let langdef = $scope.settings.languages[i];

    if (langdef.key === language) {
      $scope.settings.selectedLang = langdef;
      break;
    }
  }

  $scope.changeApiUrl = function() {
    localStorage.setItem('apiurl', $scope.settings.apiUrl);
  };

  $scope.changeLanguage = function() {
    appModalService
      .show('templates/singleSelect.html', 'SingleSelectCtrl', {
        titleId: 'MODAL_SELECT_LANGUAGE',
        items: $scope.settings.languages,
        selectedItem: $scope.settings.selectedLang
      })
      .then(function(result) {
        $scope.settings.selectedLang = result;

        $translate.use($scope.settings.selectedLang.key);
        localStorage.setItem('language', $scope.settings.selectedLang.key);
      }, function(err) {
        // error
      });
  };
}
