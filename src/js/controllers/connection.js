export default function connectionCtrl($scope, $translate, parameters) {
  $scope.data = {
    connection: parameters.connection,
    toggled: {}
  };

  for (let i = 0; i < $scope.data.connection.sections.length; i++) {
      let section = $scope.data.connection.sections[i];
      section.id = i;

      $scope.data.toggled[i] = false;
  }

  $scope.ok = function() {
    $scope.closeModal()
  };

  $scope.toggleDetails = function(section) {
      $scope.data.toggled[section.id] = !$scope.data.toggled[section.id];
  }
}
