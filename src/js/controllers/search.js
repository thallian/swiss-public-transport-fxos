import moment from 'moment';
import Api from '../transport';

export default function searchCtrl($scope, $translate, $state, $ionicLoading, $ionicPopup, appModalService) {
  let now = moment().seconds(0).millisecond(0);

  $scope.search = {
    from: '',
    to: '',
    date: now.toDate(),
    time: now.toDate(),
    isArrivalTime: false
  };

  $scope.connections = function() {
    let api = new Api();

    if (api.reachable()) {
      let locationFrom = '';
      let locationTo = '';

      let searchLocations = function(location, titleId, callback) {
        let selectedLocation = {
          name: location
        };

        api.locations(location).then(function(locations) {
          selectedLocation = locations[0];
          if (locations.length > 1) {
            $ionicLoading.hide();
            appModalService
              .show('templates/singleSelect.html', 'SingleSelectCtrl', {
                titleId: titleId,
                items: locations
              })
              .then(function(result) {
                $ionicLoading.show();
                callback(result);
              }, function(err) {
                // error
              });
          } else {
            callback(selectedLocation);
          }
        }).catch(function(exception) {
          console.log(exception);

          let errorMSg = exception.error === 'loaderror' ? $translate.instant('MODAL_TEXT_UNREACHABLE'): exception.error;

          $ionicPopup.alert({
            title: 'Error',
            template: errorMSg
          });
          $ionicLoading.hide();
        });
      }

      $ionicLoading.show();
      searchLocations($scope.search.from, 'MODAL_SELECT_DEPARTURE', function(fromLocation) {
        $scope.search.from = fromLocation.name;

        searchLocations($scope.search.to, 'MODAL_SELECT_ARRIVAL', function(toLocation) {
          $scope.search.to = toLocation.name;
          $ionicLoading.hide();

          let timestamp = new Date(
            $scope.search.date.getFullYear(),
            $scope.search.date.getMonth(),
            $scope.search.date.getDate(),
            $scope.search.time.getHours(),
            $scope.search.time.getMinutes(),
            $scope.search.time.getSeconds()).getTime() / 1000;

          $state.go('app.connections', {
            from: $scope.search.from,
            to: $scope.search.to,
            timestamp: timestamp,
            isArrivalTime: $scope.search.isArrivalTime ? '1' : '0'
          });
        });
      });
    } else {
      let errorMSg = $translate.instant('MODAL_TEXT_UNREACHABLE');
      $ionicPopup.alert({
        title: 'Error',
        template: errorMSg
      });
    }
  };
};
