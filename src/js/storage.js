import Dexie from 'dexie';

export default class Storage {
  init() {
    let apiurl = localStorage.getItem('apiurl');
    let language = localStorage.getItem('language');

    if (apiurl === null) {
        localStorage.setItem('apiurl', 'http://transport.opendata.ch/v1/');
    }

    if (language === null) {
        let sysLang = navigator.language.split('-')[0];
        localStorage.setItem('language', sysLang);
    }

    /*this.db = new Dexie('spt');
    this.db.version(0.1)
      .stores({
        config: 'language,apiurl'
      });

    this.db.open()
      .catch(function(error) {
        console.error(error);
    });*/
  }
}
