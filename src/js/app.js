import Translate from 'angular-translate';
import StaticLoader from 'angular-translate-loader-static-files';
import Storage from './storage';
import ModalService from './modalService'
import Filters from './filters'
import Controllers from './controllers'

angular.module('spt', ['ionic', 'spt.controllers', 'spt.filters', 'pascalprecht.translate'])

.factory('appModalService', ['$ionicModal', '$rootScope', '$q', '$injector', '$controller', ModalService])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.constant('$ionicLoadingConfig', {
  template: '<ion-spinner></ion-spinner>'
})

.config(function($stateProvider, $urlRouterProvider, $compileProvider, $translateProvider, $ionicConfigProvider) {
  // add 'app:'' to the whitelist for FirefoxOS
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|app):/);
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|app):/);

  $ionicConfigProvider.backButton.text('').icon('ion-chevron-left').previousTitleText(false);

  let storage = new Storage();
  storage.init();

  $translateProvider.useSanitizeValueStrategy('escape');

  $translateProvider.useStaticFilesLoader({
    prefix: '/locales/',
    suffix: '.json'
  });
  $translateProvider.preferredLanguage(localStorage.getItem('language'));

  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html',
        controller: 'SearchCtrl'
      }
    }
  })

  .state('app.connections', {
    url: '/connections/:from/:to/:timestamp/:isArrivalTime',
    views: {
      'menuContent': {
        templateUrl: 'templates/connections.html',
        controller: 'ConnectionsCtrl'
      }
    }
  })

  .state('app.settings', {
      url: '/settings',
      views: {
        'menuContent': {
          templateUrl: 'templates/settings.html',
          controller: 'SettingsCtrl'
        }
      }
    })
    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
        }
      }
    })
    .state('app.license', {
      url: '/license',
      views: {
        'menuContent': {
          templateUrl: 'templates/license.html',
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/search');
});
