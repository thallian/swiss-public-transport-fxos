import appCtrl from './controllers/app'
import searchCtrl from './controllers/search'
import connectionsCtrl from './controllers/connections'
import connectionCtrl from './controllers/connection'
import settingsCtrl from './controllers/settings'
import singleSelectCtrl from './controllers/singleSelect'

angular.module('spt.controllers', [])

.controller('AppCtrl', appCtrl)
.controller('SearchCtrl', searchCtrl)
.controller('ConnectionsCtrl', connectionsCtrl)
.controller('ConnectionCtrl', connectionCtrl)
.controller('SettingsCtrl', settingsCtrl)
.controller('SingleSelectCtrl', singleSelectCtrl);
