# ATTENTION

I no longer maintain this as I don't use Firefox OS anymore.

# Motivation

I wanted a free (as in freedom and in beer) app to get myself
the information about train schedules in Switzerland on my Firefox OS device.

As such a thing did not exist I tried to build something useful and even learn
a thing or two on the way. The result is an ongoing stream of ideas and my clumsy
tries implementing them.

# Transport API

To get the schedule information the fine [Transport API](http://transport.opendata.ch/)
from [opendata.ch](http://opendata.ch) is used.
It is trivial to run the API server for yourself. That is in fact what I am doing
and that's also the standard server the app points to.

The API server can be configured in the settings dialog.

# Privacy

The app does not under any circumstances collect any personal information.
The only info you are giving away is what is sent to the api server. If you
want to have this under control you have the possibility to host your own.

The default server at https://transport.vanwa.ch/ does not record any data
(ips are never stored and logs are regularly purged).

# Licensing

This project (Swiss Public Transport App) is licensed under the Mozilla Public License 2.0
as written in the `LICENSE` file.

## Used Projects

- mithril.js: MIT
- l20n.js: Apache License 2.0
- localforage.js: Apache License 2.0
- moment.js: MIT
- react.js: Apache License 2.0
- Gaia CSS: Apache License 2.0

A big thanks to all the maintainers of these libraries!

# Development

Main development happens at https://code.vanwa.ch/shu/swiss-public-transport-fxos but there is a
mirror of the git repository on [Github](https://github.com/thallian/spt_fxos).

# Building

## Prerequisites

- node
- npm
- gulp

Clone the git repository and do an `npm install` followed
by `gulp clean && gulp dev`.

This installs all dependencies, builds the whole stuff and watches for changes.
The app can now be tested by opening index.html in the `dist` folder.

# Versions
## 0.3

- rewrite with ionic/angular (yeah, yeah I should make up my mind already)

## 0.2

- rewrite using the [mithril](http://lhorie.github.io/mithril/) framework
  (won't be the last one)
- connection screen now only shows an overview
- connection detail screen looks way better
- possibility to show the stopovers
- header stays fixed

## 0.1.1

- French and spanish translation
- Ability to read the license (MPL 2.0) in the about screen
- Fixed the layout on older FxOS versions `(< 1.4)`

## 0.1

- First release
