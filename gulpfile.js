var gulp = require('gulp');
var webserver = require('gulp-webserver');
var clean = require('./gulp/clean');
var packaging = require('./gulp/package');

var browserify = require('./gulp/browserify').bind(this, []);
var stylus = require('./gulp/stylus').bind(this, ['./vendor/ionic/ionic.app.css']);

gulp.task('clean', clean);
gulp.task('browserify', browserify);
gulp.task('stylus', stylus);
gulp.task('build', ['clean', 'stylus'], browserify);
gulp.task('package', ['build'], packaging.bind(this, 'spt_fxos.zip'));

// Watch
gulp.task('watch', ['build'], function () {
  gulp.watch([
    './src/**/*.{js,json,html,css,styl,png,webapp}',
  ],['build']);
});

// Serve + Watch
gulp.task('dev', ['watch'], function() {
  gulp.src('dist')
    .pipe(webserver({
      port: 9090,
      livereload: true,
      fallback: 'index.html'
    }));
});
